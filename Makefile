.DEFAULT_GOAL := help

DESTDIR ?= ./build
BIN ?= $(DESTDIR)/usr/bin
MAN ?= $(DESTDIR)/usr/share/man/man1
DOC ?= $(DESTDIR)/usr/share/doc/timevox

.PHONY: help
help:
	@echo "Specify a target to build:"
	@echo "  -> make install"
	@echo "     install timevox in DESTDIR (default: ./build)"
	@echo "  -> make clean"
	@echo "     remove built files from DESTDIR"

.PHONY: clean
clean:
	@echo "Cleaning $(DESTDIR)..."
	rm -rf $(DESTDIR)/*

.PHONY: build
build:
	@echo "timxvox really doesn't need building, it's a bash script!"

.PHONY: install
install:
	@echo "Installing timevox in $(DESTDIR)..."
	install -Dcm 755 timevox.sh $(BIN)/timevox
	install -Dcm 644 timevox.1 $(MAN)/timevox.1
	install -Dcm 644 LICENSE $(DOC)/LICENSE