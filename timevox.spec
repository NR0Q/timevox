Name:           timevox
Summary:        Announces the time
Version:        0.4.2
Release:        1%{?dist}
BuildArch:      noarch
Source0:        %{name}-%{version}.tar.gz
License:        BSD 3-Clause
URL:            https://gitlab.com/NR0Q/timevox
Group:          Utilities
Packager:       Matthew Chambers <nr0q@gridtracker.org>
Requires:       alsa-utils espeak-ng coreutils
BuildRequires:  make

%description
This is a little script that is intended to run at about 5 seconds
before the top of every hour, it grabs the current time, optionally
rounds up to the next minute or hour and announces it over the
system's default audio output with a following tone. Much in the
way you'd hear the time from NIST station WWV.

%prep
%setup -q timevox

%build

%install
DESTDIR=${RPM_BUILD_ROOT} make install

%check

%clean
DESTDIR=${RPM_BUILD_ROOT} make clean

%files
%{_bindir}/%{name}
%{_mandir}/man1/
%license %{_docdir}/%{name}/

%changelog
* Wed Dec 08 2021 Matthew Chambers <nr0q@gridtracker.org> - 0.4.3-1
- Fix Debian builds deploying to wrong directory, new beta version
* Wed Dec 08 2021 Matthew Chambers <nr0q@gridtracker.org> - 0.4.2-1
- Fix coreutils-single to just coreutils to fix broken bad dependency
* Tue Dec 07 2021 Matthew Chambers <nr0q@gridtracker.org> - 0.4.1-1
- Small bits of cleanup - first version to release to beta
* Tue Dec 07 2021 Matthew Chambers <nr0q@gridtracker.org> - 0.4.0-1
- First pre-release of timevox