# timevox

timevox.sh is Copyright 2022 Matthew Chambers and is available
freely under the terms of the BSD 3-Clause license.

This is a little script that is intended to run at about 5 seconds before the top of every hour, it grabs the current time, optionally rounds up to the next minute or hour and announces it over the system's default audio output with a following tone. Much in the way you'd hear the time from NIST station WWV. 

## Usage:
- -a  Append text to end of message (such a ham repeater ID).
- -h  Display usage message.
- -H  Announce upcoming top of hour time (upcoming hour and 00 minutes).  Can not be used with -M"
- -M  Announce upcoming minute (useful for running as a cron job just before the new minute).  Can not be used with -H.
- -t  Set the tone frequency in Hz between 30 and 8000 (Default 1000Hz).
- -T  Omit the tone, just announce the time.
- -v  Display the program version.
- -V  Set the tone volume percentage between 1 and 100 (Default 50%).
- -z  Announce UTC time.